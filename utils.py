import os
from twython import Twython

basedir = os.path.abspath(os.path.dirname(__file__))


def get_env_variable(name):
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)


twitter = Twython(get_env_variable('CONSUMER_KEY'), get_env_variable('CONSUMER_SECRET'))
