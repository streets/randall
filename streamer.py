from twython import TwythonStreamer
import csv
import json
from utils import get_env_variable, basedir
from selenium import webdriver
from time import sleep
import pathlib
import os

with open("targets.json", "r") as file:
    accounts = json.load(file)

follow_list = []
for account in accounts:
    follow_list.append(account["user_id"])

# Filter out unwanted data
def process_tweet(tweet):
    d = {}
    d['created'] = tweet['created_at']
    d['userid'] = tweet['user']['id']
    d['user'] = tweet['user']['screen_name']
    d['text'] = tweet['text']
    if "media" in tweet['entities']:
        d['media'] = [media['media_url_https'] for media in tweet['entities']['media']]
    d['client'] = tweet['source']
    d['user_loc'] = tweet['user']['location']
    d['user_timezone'] = tweet['user']['time_zone']
    d['status_url'] = 'https://twitter.com/{}/status/{}?s=20'.format(d['user'],tweet['id'])
    d['status_id'] = tweet['id']
    return d
    
# Create a class that inherits TwythonStreamer
class MyStreamer(TwythonStreamer):     

    # Received data
    def on_success(self, data):

        # Only collect tweets in English
        if 'lang' in data:
            if data['lang'] == 'en':
                
                # Only collect tweets made by the Target (not in reply to, which is Twitter API Default)
                matching = [s for s in follow_list if str(data['user']['id_str']) in s]
                if matching:
                    tweet_data = process_tweet(data)
                    self.save_to_csv(tweet_data)
                    self.save_screenshot(tweet_data)

    # Problem with the API
    def on_error(self, status_code, data):
        print(status_code, data)
        self.disconnect()
        
    # Save each tweet to csv file
    def save_to_csv(self, tweet):
        
        path = '{}/targets/{}'.format(basedir, str(tweet['user']).lower())
        pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        
        filename = r'{}/tweets.csv'.format(path, tweet['status_id'])
        
        with open(filename, 'a') as file:
            writer = csv.writer(file)
            writer.writerow(list(tweet.values()))
            
    # Save each tweet to csv file
    def save_screenshot(self, tweet):
        
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument('start-maximized')
        driver = webdriver.Chrome(options=options, executable_path='/snap/bin/chromium.chromedriver')
        driver.set_window_size(1400,1000)
        
        path = '{}/targets/{}'.format(basedir, str(tweet['user']).lower())
        pathlib.Path(path).mkdir(exist_ok=True)
        
        filename = r'{}/{}.png'.format(path, tweet['status_id'])
        driver.get(str(tweet['status_url']))
        sleep(5)
        driver.save_screenshot(filename)
        driver.close()
        

# Instantiate from our streaming class
stream = MyStreamer(get_env_variable('CONSUMER_KEY'), get_env_variable('CONSUMER_SECRET'), 
                    get_env_variable('ACCESS_TOKEN'), get_env_variable('ACCESS_SECRET'))



# Start the stream
stream.statuses.filter(follow=follow_list)