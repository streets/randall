import json
from utils import twitter
import os.path
from os import path

def add_target():
    target = {}
    username = input("Enter Twitter Username of Target: @")
    target["screen_name"] = str(username)
    output = twitter.lookup_user(screen_name=str(username))
    target["user_id"] = output[0]["id_str"]

    if path.isfile('targets.json'):
        with open("targets.json", "r") as file:
            data = json.load(file)
        
        data.append(target)
        
        with open("targets.json", "w") as file:
            json.dump(data, file)
    
    else:
        data = []
        data.append(target)
        with open("targets.json", "w") as file:
            json.dump(data, file)

add_target()