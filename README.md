# Randall
A python script that watches target Twitter accounts and logs their tweets, retweets, and replies to a CSV file as well as capturing a rendered screenshot.

<img src="https://media1.tenor.com/images/c142fffab96ebaa82b1effbfa844b3ce/tenor.gif?itemid=9625258">

## Pre Install
Install and usage instructions assume you are running Ubuntu 20.04. Any linux varient should work the same. Modify/tweak as needed.


You should have pip installed

    sudo apt install python3-pip

You should have virtualenv installed

    sudo pip3 install virtualenv==20.0.23


## Install

### Create Virtual Env
```
virtualenv --python=/usr/bin/python3 venv
```

### Add Env Variables
You need API keys from Twitter. See https://developer.twitter.com/en/apps

Append the API keys to the end of `venv/bin/activate`
```
export CONSUMER_KEY=abc123
export CONSUMER_SECRET=abc123
export ACCESS_TOKEN=abc123-456
export ACCESS_SECRET=abc123
```

### Activate Virtual Env
```
source venv/bin/activate
```

### Install Requirements
```
pip install -r requirements.txt
```
```
snap install chromium
```

## Usage

### Add Targets
```
python addtarget.py
```

The `addtarget.py` tool will create or modify an existing `targets.json` file in the directory with the Twitter username and internal Twitter id of a target to track.

The `targets.json` can be manually created or managed. The format is as follows:
```
[{"screen_name": "<twitter_user_name>", "user_id": "<twitter_user_id>"}]
```

### Start Streamer
```
python streamer.py
```

The streamer will run silently, looking for tweets by the targets to log.


### Check Results
Captured tweets will be stored in a director called `targets`. 

```
Randall
│
└───targets
│   │
│   └───<userName>
│       │   tweets.csv
│       │   <tweetid>.png
│       │   ...
```